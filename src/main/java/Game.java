import java.util.*;

public class Game {

    private Board board = new Board();
    private Color currentColor = new Random().nextBoolean() ? Color.R : Color.G;
    private List<Move> history = new ArrayList<>();
    private UUID id;

    public Game() {
        this.id = UUID.randomUUID();

    }

    public Game(UUID id) {
        this.id = id;


    }

    public void makeMove(Move move) {
        this.board.setColorInColumn(move.column, move.color);
        history.add(move);
        if(!move.color.isWinner) {
            this.setNextColor();
        }
    }

    public boolean isRunning() {
        return !currentColor.isWinner;
    }

    public Color getCurrentColor() {
        return currentColor;
    }

    public void setNextColor() {
        this.currentColor = this.currentColor.getNextColor();
    }

    public boolean isMoveLegal(Move move) {
        return !board.isColumnOutOfBounds(move.column) && !board.isColumnFull(move.column) && isRunning();
    }

    public static Game restoreGame(Game game, boolean replay) {
       return Game.restoreGame(game, replay, game.getHistory().size());
    }

    public static Game restoreGame(Game game, boolean replay, int to) {

        Game newGame;
        List<Move> gameHistory = game.getHistory();
        System.out.println("To: " + to);
        if(replay) {
            newGame = new Game(game.getId());
        } else {
           newGame = new Game();
        }
        if(to < 0) {
            to = 0;
        }
        if(to > gameHistory.size() - 1) {
            to = gameHistory.size() - 1;
        }

        newGame.board = Board.recreateBoard(gameHistory,to);
        Color current = to == 0 ? game.getCurrentColor() : gameHistory.get(to - 1).color.getNextColor();
        newGame.currentColor = current;
        List<Move> subHistory = new ArrayList<>(gameHistory.subList(0, to));
        newGame.history = subHistory;
        return newGame;


    }
    public List<Move> getHistory() {
        return this.history;
    }

    public UUID getId() {
        return this.id;
    }


    public Board getBoard() {
        return this.board;
    }


}
