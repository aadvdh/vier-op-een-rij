import java.util.Arrays;
import java.util.List;

public class Board {
    private final int COLUMNS = 7;
    private final int ROWS = 6;
    Color[][] board = new Color[ROWS][COLUMNS];

    public Board() {
        for(Color[] row : board) {
            Arrays.fill(row, Color.Leeg);
        }
    }

    public boolean isColumnFull(int column) {
        if(isColumnOutOfBounds(column)) {
            throw new IndexOutOfBoundsException("Kolom moet minimaal 0 zijn en maximaal 6");
        }
        int count = 0;
        for(int i = 0; i < this.ROWS; i++) {
            if(this.board[i][column] != Color.Leeg) {
                count++;
            }
        }
        return count == this.ROWS;
    }

    private int emptySpotInColumn(int column) {
        if(isColumnOutOfBounds(column)) {
            throw new IndexOutOfBoundsException("Kolom moet minimaal 0 zijn en maximaal 6");
        }

        //Zoekt van beneden naar boven voor een lege plek
        for(int i = ROWS - 1; i >= 0 ; i--) {
            if(this.board[i][column] == Color.Leeg) {
               return i;
            }
        }

        return -1;
    }

    public boolean isColumnOutOfBounds(int column) {
        if(column < 0 || column > COLUMNS - 1) {
            return true;
        }
        return false;
    }

    public boolean isRowOutOfBounds(int row) {
        if(row < 0 || row > ROWS - 1 ) {
            return true;
        }
        return false;
    }


    public void setColorInColumn(int column, Color color) {
        //Zoek de lege plek in de Kolom
        int emptySpot = emptySpotInColumn(column);

        //Plaats de steen van de huidige speler in de lege plek van de gegeven kolom
        this.board[emptySpot][column] = color;

        //Niet elke positie hoeft gecheckt te worden, alleen 3 stenen in alle richtingen van de geplaatste steen
        int startRow = isRowOutOfBounds(emptySpot - 3) ? 0 : emptySpot - 3;
        int endRow = isRowOutOfBounds(emptySpot + 3) ? ROWS - 1 : emptySpot + 3;
        int startColumn = isColumnOutOfBounds(column - 3) ? 0 : column - 3;
        int endColumn = isColumnOutOfBounds(column + 3) ? COLUMNS - 1 : column + 3;

        //Check of speler gewonnen heeft
        if(didPlayerWin(color, startRow, endRow, startColumn, endColumn)) {
            color.isWinner = true;
        }
    }


    //Checkt voor een startX tot endX en startY tot endY voor elke positie daarin in alle richtingen of er vier stenen op een rij liggen
    private boolean didPlayerWin(Color color, int startX, int endX, int startY, int endY) {

        //0,1: Verticaal
        //1,0: Horizontaal
        //1,-1: Anti Diagonaal
        //1,1: Diagonaal
        int[][] directions = {{0,1}, {1,-1}, {1,1}, {1,0}};
        for(int[] direction : directions) {

            for (int x = startX; x <= endX; x++) {
                int dx = direction[0];
                int dy = direction[1];

                for (int y = startY; y <= endY; y++) {
                    int lastX = x + dx * 3;
                    int lastY = y + dy * 3;
                    if(!isRowOutOfBounds(lastX) && !isColumnOutOfBounds(lastY)) {
                        if(board[x+dx][y+dy] == color && board[x+2*dx][y+2*dy] == color && board[lastX][lastY] == color && board[x][y] == color) {
                            return true;
                        }
                    }

                }

            }
        }
        return false;
    }

    public Board recreateBoard(List<Move> moves)  {
        return recreateBoard(moves, moves.size() - 2);
    }

    public static Board recreateBoard(List<Move> moves, int to) {



        Board board =  new Board();
        for(int i = 0; i < to; i++) {
            board.setColorInColumn(moves.get(i).column, moves.get(i).color);
        }

        return board;

    }



    public String toString() {
        int count = 6;
        String[]  chars = {"a", "b", "c", "d", "e", "f", "g"};
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < this.board[0].length; i++ ) {
            if(i == 0) {
                builder.append(" ");
            }
            builder.append(chars[i]);
            builder.append(" ");
        }
        builder.append("\n");
        for(Color[] row: this.board) {
            int rowCount = 0;
            for(Color col : row) {
                if(rowCount == 0) {
                    builder.append("|");
                }
                rowCount++;
                builder.append(col);
                builder.append("|");
            }
            builder.append(" ");
            builder.append(count);
            count--;
            builder.append("\n");
        }
        for(int i = 0; i < 7; i++) {
            builder.append(" ");
            builder.append("_");
        }
        return builder.toString();
    }










}
