public enum Color {
    R, G, Leeg {
        @Override
        public String toString() {
            return " ";
        }
    };
    public boolean isWinner = false;

    public Color getNextColor() {
        return this == Color.R ? Color.G : Color.R;
    }
}
