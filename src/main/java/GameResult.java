import java.util.Date;
import java.util.List;
import java.util.UUID;

public class GameResult implements Comparable<GameResult>{

    private Game game;
    private Color winner;
    private Date playedAt;

    public GameResult(Game game, Color winner) {
        this.game = game;
        this.winner = winner;
        this.playedAt = new Date(System.currentTimeMillis());
    }

    public Game getGame() {
        return game;
    }


    public int compareTo(GameResult other) {
        return this.playedAt.compareTo(other.getPlayedAt());
    }




    public Color getWinner() {
        return winner;
    }

    public Date getPlayedAt() {
        return this.playedAt;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Gespeeld op: " + this.playedAt);
        builder.append("\t");
        builder.append("GameId: " + game.getId());
        builder.append("\t");
        builder.append("Winnaar: " + this.winner);
        return builder.toString();
    }


}
