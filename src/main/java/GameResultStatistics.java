import java.util.List;

public class GameResultStatistics {

    private List<GameResult> results;

    public GameResultStatistics(List<GameResult> results) {
        this.results = results;
    }

    public int getWinCountByColor(Color color) {
        int count = 0;
        for(GameResult result : results) {
            if(result.getWinner() == color) {
                count++;
            }
        }
        return count;
    }

    public int getAverageMovesPerGame() {
        double average = results.stream().mapToInt(result -> result.getGame().getHistory().size()).average().orElse(0.0);
        return (int) average;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Gewonnen door rood: " + getWinCountByColor(Color.R));
        builder.append("\n");
        builder.append("Gewonnen door geel: " + getWinCountByColor(Color.G));
        builder.append("\n");
        builder.append("Gemiddeld aantal beurten per spel: " + getAverageMovesPerGame());
        return builder.toString();
    }
}
