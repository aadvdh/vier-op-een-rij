import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public class VierOpEenRij {

    private final static Scanner in = new Scanner(System.in);
    private static Game game = new Game();
    private static Map<UUID, GameResult> gameResults = new HashMap<>();

    public static void main(String args[]) throws InterruptedException {
        while(true) {
            boolean playAgain = true;
            while(true) {
                System.out.println("0: Speel een nieuwe ronde");
                System.out.println("1: Herspeel een vorige game vanaf een opgegeven beurt");
                System.out.println("2: Bekijk de resultaten van de gespeelde games");
                System.out.println("3: Bekijk de statistieken van de gespeelde games");
                System.out.println("4: Bekijk een replay van een game");
                System.out.println("X: Sluit het programma af");
                String command = in.nextLine();
                if(command.equals("0")) {

                    game = new Game();
                    break;
                }
                if(command.equals("X")) {
                    playAgain = false;
                    break;
                }


                if(command.equals("1")) {
                    System.out.println("Voer gameId in: ");
                    UUID gameId = UUID.fromString(in.nextLine());
                    GameResult res = gameResults.get(gameId);
                    if(res == null) {
                        System.out.println("Ongeldige gameId");
                        break;
                    }
                    System.out.println("0: Nieuwe game 1: Herspeel");
                    String replayString = in.nextLine();
                    boolean replay = replayString.equals("1") ? true : false;
                    System.out.println("Vanaf?");
                    int to = Integer.valueOf(in.nextLine());
                    game =  Game.restoreGame(res.getGame(), replay, to);
                    break;
                }

                if(command.equals("2")) {
                    System.out.println("Resultaten: ");
                    ArrayList<GameResult> results = new ArrayList<>(gameResults.values());
                    Collections.sort(results);
                    for(GameResult result : results) {
                        System.out.println(result.getGame().getBoard());
                        System.out.println(result);
                    }
                }
                if(command.equals("3")) {
                    System.out.println("Statistieken: ");
                    GameResultStatistics stats = new GameResultStatistics((new ArrayList<>(gameResults.values())));
                    System.out.println(stats);
                }

                if(command.equals("4")) {
                    System.out.println("Voer gameId in: ");
                    UUID gameId = UUID.fromString(in.nextLine());

                    GameResult res = gameResults.get(gameId);
                    if(res == null) {
                        System.out.println("Ongeldige gameId");
                        break;
                    }
                    Game game = new Game();
                    List<Move> moves = res.getGame().getHistory();
                    for(Move move : moves) {
                        game.getBoard().setColorInColumn(move.column, move.color);
                        System.out.println(game.getBoard());
                        Thread.sleep(1000);
                    }
                    game.getCurrentColor().isWinner = false;
                    System.out.println(res.getWinner() + " had Game: " + res.getGame().getId() + " gewonnen.");
                }
                System.out.println("");

            }
            if(playAgain) {


                while (game.isRunning()) {
                    System.out.println("Is running");
                    System.out.println(game.getBoard());

                    informPlayersOfTurn();
                    makeNextMove();
                    System.out.println(game.getHistory().size());
                }
                GameResult result = new GameResult(game, game.getCurrentColor());
                gameResults.put(game.getId(), result);
                System.out.println(game.getBoard());
                System.out.println(result);
                game.getCurrentColor().isWinner = false;
            } else {
                System.out.println("Bedankt voor het spelen!");
                break;
            }



        }

    }

    public static void informPlayersOfTurn() {
        System.out.println(game.getCurrentColor() + " is aan de beurt");
    }

    public static void makeNextMove() {
        Move move = askForMove();
        while(!game.isMoveLegal(move)) {
            System.out.println("Kolom zit vol of je probeert de steen in een niet bestaande kolom te gooien");
            move = askForMove();
        }
        game.makeMove(move);

    }

    private static Move askForMove() {
        System.out.println("Geef de letter van de kolom waarin je de steen wilt gooien");
        String column = in.nextLine();
        int columnIndex = letterToColumnIndex(column);
        return new Move(columnIndex, game.getCurrentColor());
    }

    private static int letterToColumnIndex(String letter) {
        if ("a".equals(letter)) {
            return 0;
        } else if ("b".equals(letter)) {
            return 1;
        } else if ("c".equals(letter)) {
            return 2;
        } else if ("d".equals(letter)) {
            return 3;
        } else if ("e".equals(letter)) {
            return 4;
        } else if ("f".equals(letter)) {
            return 5;
        } else if ("g".equals(letter)) {
            return 6;
        }
        return -1;

    }



}



